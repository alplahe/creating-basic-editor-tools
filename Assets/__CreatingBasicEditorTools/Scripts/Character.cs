﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	public Texture2D m_portrait;
	public string m_nickname;
	public Color m_color;

	public PlayerMovement m_playerMovement;
}
