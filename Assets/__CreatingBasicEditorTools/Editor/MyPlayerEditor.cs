﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof (MyPlayer), true)]
[CanEditMultipleObjects]
public class MyPlayerEditor : Editor 
{

	SerializedProperty m_damageProp;
	SerializedProperty m_armorProp;
	SerializedProperty m_gunProp;
	SerializedProperty m_miauProp;

    
	void OnEnable () 
	{
		// Setup the SerializedProperties
		m_damageProp = serializedObject.FindProperty ("m_damage");
		m_armorProp = serializedObject.FindProperty ("m_armor");
		m_miauProp = serializedObject.FindProperty ("m_miau");
		m_gunProp = serializedObject.FindProperty ("m_gun");
	}

	public override void OnInspectorGUI()
	{
		// Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
		serializedObject.Update ();
		// Show the custom GUI controls
		EditorGUILayout.IntSlider (m_damageProp, 0, 100, new GUIContent ("Damage"));
		// Only show the damage progress bar if all the objects have the same damage value:
		if (!m_damageProp.hasMultipleDifferentValues)
			ProgressBar (m_damageProp.intValue / 100.0f, "Damage");
		EditorGUILayout.IntSlider (m_armorProp, 0, 100, new GUIContent ("Armor"));
		// Only show the armor progress bar if all the objects have the same armor value:
		if (!m_armorProp.hasMultipleDifferentValues)
			ProgressBar (m_armorProp.intValue / 100.0f, "Armor");
		EditorGUILayout.IntSlider (m_miauProp, 0, 100, new GUIContent ("Miau"));
		// Only show the armor progress bar if all the objects have the same armor value:
		if (!m_armorProp.hasMultipleDifferentValues)
			ProgressBar (m_miauProp.intValue / 100.0f, "Miau");
		EditorGUILayout.PropertyField (m_gunProp, new GUIContent ("Gun Object"));
		// Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
		serializedObject.ApplyModifiedProperties ();
	}

	// Custom GUILayout progress bar.
	void ProgressBar (float value, string label)
	{
		// Get a rect for the progress bar using the same margins as a textfield:
		Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
		EditorGUI.ProgressBar (rect, value, label);
		EditorGUILayout.Space ();
	}
}