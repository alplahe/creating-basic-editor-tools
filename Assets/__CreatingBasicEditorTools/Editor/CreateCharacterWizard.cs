﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CreateCharacterWizard : ScriptableWizard
{
    public Texture2D m_portraitTexture;
    public Color m_color = Color.white;
    public string m_nickname = "Default nickname";
    
    [MenuItem("My Tools/Create Character Wizard...")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<CreateCharacterWizard>("Create Character", "Create new", "Update selected");
    }

    private void OnWizardCreate()
    {
        GameObject characterGameObject = new GameObject();

        Character characterComponent = characterGameObject.AddComponent<Character>();
        AssignCharacterFields(characterComponent);

        AssignPlayerMovementField(characterGameObject, characterComponent);
    }

    private void AssignCharacterFields(Character characterComponent)
    {
        characterComponent.m_portrait = m_portraitTexture;
        characterComponent.m_color = m_color;
        characterComponent.m_nickname = m_nickname;
        characterComponent.gameObject.name = m_nickname;
    }

    private static void AssignPlayerMovementField(GameObject characterGameObject, Character characterComponent)
    {
        PlayerMovement playerMovement = characterGameObject.AddComponent<PlayerMovement>();
        characterComponent.m_playerMovement = playerMovement;
    }

    private void OnWizardOtherButton()
    {
        if (Selection.activeTransform != null)
        {
            GetCharacterComponentFromSelectionAndAssignFields();
        }
    }

    private void GetCharacterComponentFromSelectionAndAssignFields()
    {
        Character characterComponent = Selection.activeTransform.GetComponent<Character>();

        if (characterComponent != null)
        {
            AssignCharacterFields(characterComponent);
        }
    }

    private void OnWizardUpdate()
    {
        helpString = "Enter character details";
    }
}
